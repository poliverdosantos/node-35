const { generateSuma, generateResta, generateMultiplicacion, generateDivision } = require('./files/operaciones');
const argv = require('yargs').argv;



console.log('=========================================================');
console.log('          OPERACIONES  ALGEBRAICAS  CON  YARGS           ');
console.log('=========================================================');

// console.log(process.argv);
// console.log(argv);

// console.log('base:yargs',argv.base);

generateSuma(argv.base);
generateResta(argv.base);
generateMultiplicacion(argv.base);
generateDivision(argv.base);